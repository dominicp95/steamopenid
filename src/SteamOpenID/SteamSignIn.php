<?php
namespace SteamOpenID;

class SteamSignIn
{
	const STEAM_LOGIN = 'https://steamcommunity.com/openid/login';
        public $domain;
        private $debug;
        
        public function __construct( $domain, $debug = false ){
            $this->domain = $domain;
            $this->debug = $debug;
        }
        
	/**
	* Get the URL to sign into steam
	*
	* @param mixed returnTo URI to tell steam where to return, MUST BE THE FULL URI WITH THE PROTOCOL
	* @param bool useAmp Use &amp; in the URL, true; or just &, false. 
	* @return string The string to go in the URL
	*/
	public function genUrl(){
            $openid = new LightOpenID( $this->domain, $this->debug ); //This is root url used to generate return url, so the url you will be redirected after click "sign in" on steam.
            $openid->identity = 'http://steamcommunity.com/openid';
            return $openid->authUrl();
	}
	
	/**
	* Validate the incoming data
	*
	* @return string Returns the SteamID64 if successful or empty string on failure
	*/
	public function validate(){
            $openid = new LightOpenID( $this->domain, $this->debug ); 
            if($this->debug){ var_dump($openid); }
            
            $openid->identity = 'http://steamcommunity.com/openid';
            
            if($openid->mode == 'cancel'){
                return 'Login canceled.<br/>';
            }else if($openid->mode == 'error'){
                return 'Login error.<br/>';
            }else if($openid->mode == 'id_res') {
                if($openid->validate()){
                    $re = '/openid\/id\/([0-9]+)$/'; 
                    if(preg_match($re, $openid->identity, $matches)){
                        return (int)$matches[1];	
                    } else {
                        return 'Unsuported identity format, steamid not found.';
                    }
                } else {
                    return 'Login failed!<br/>';
                }
            }elseif($openid->mode){ //Just in case, to discover modes I don't know of, and maybe handle them.
                return 'Unsoported mode: '.$openid->mode.'<br/>';
            }
            return false;
	}
}