<?php
namespace SteamOpenID;

class SteamUser
{
    private $apikey; 
    private $domain;
    private $redirurl;
    private $steamsignin;
    
    private $lastError;
    
    public function __construct( $domain, $redirurl, $apikey="", $debug = false ){
        $this->domain = $domain;
        $this->apikey = $apikey;
        $this->redirurl = $redirurl;
        
        $this->lastError = "";
        
        $this->steamsignin = new SteamSignIn( $this->domain, $debug );
    }
    
    public function GetPlayerSummaries ($steamid)
    {
        $response = file_get_contents('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=' . $this->apikey . '&steamids=' . $steamid);
        $json = json_decode($response);
        return $json->response->players[0];
    }

    public function validate ()
    {
        $res = $this->steamsignin->validate();
        
        if( is_int( $res ) ){
            return $res;
        }else{
            $this->lastError = $res;
            return false;
        }
    }
    
    public function getLoginUrl(){
        return $this->steamsignin->genUrl();
    }
    
    public function getLastError(){
        return $this->lastError;
    }
}